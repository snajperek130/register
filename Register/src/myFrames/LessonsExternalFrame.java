package myFrames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import javax.swing.text.Caret;

import Helpfull.View;
import oracle.sql.TIMESTAMP;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class LessonsExternalFrame extends JFrame {

	private JPanel contentPane;
	private JTextField name;
	private JTextField day;
	
	private Statement statement;
	private Connection connection;
	private JTable table;
	TableModel tableModel;
	private BigDecimal id;
	private JTextField month;
	private JTextField year;
	private JTextField hour;
	private JLabel lblMinute;
	private JLabel lblSecond;
	private JTextField second;
	private JTextField minute;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventsExternalFrame frame = new EventsExternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LessonsExternalFrame() {
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(244,244,244));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(54, 62, 101, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Day");
		lblNewLabel_2.setBounds(54, 90, 101, 14);
		contentPane.add(lblNewLabel_2);
		
		name = new JTextField();
		name.setBounds(158, 59, 163, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		day = new JTextField();
		day.setBounds(158, 87, 163, 20);
		contentPane.add(day);
		day.setColumns(10);
		
		JButton save = new JButton("Save");
		save.setBounds(351, 116, 89, 23);
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(getTitle().equals("Edit"))
				{
					
				
				
					CallableStatement cs = null;		
					  try { 
						  if (name.getText().equals("")||year.getText().equals("")||month.getText().equals("")||day.getText().equals("")
									||hour.getText().equals("")||minute.getText().equals("")||second.getText().equals("")) { 
										JOptionPane.showMessageDialog(null, "Fill all fields!");
									}
						  else{
							  
							  cs = connection.prepareCall( "{call lessons_package.edit_lesson(?,?,?)}");
							
							  cs.setBigDecimal(1, id);
							  cs.setString(2, name.getText());
							  
							  Timestamp ts;
							  try{
								 ts = new Timestamp(Integer.parseInt(year.getText())-1900, Integer.parseInt(month.getText())-1, Integer.parseInt(day.getText()), Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()),Integer.parseInt(second.getText()), 0);
								 cs.setTimestamp(3, ts); 
								
							  }
							  catch(NumberFormatException e1){
								  //JOptionPane.showMessageDialog(null,e1.getMessage()); 
							  }
							//  cs.setString(4,c);
							  cs.execute();
							  JOptionPane.showMessageDialog(null, "You have eddited a new lesson");
							  View.updateTheLessonsView(statement,table, tableModel);
						  }
					
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  //JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  
				}
				
				else // add
				{
					
					CallableStatement cs = null;
					//System.out.println(c);
				  try { 
					  if (name.getText().equals("")||year.getText().equals("")||month.getText().equals("")||day.getText().equals("")
						||hour.getText().equals("")||minute.getText().equals("")||second.getText().equals("")) { //||c.equals("")
							JOptionPane.showMessageDialog(null, "Fill all fields!");
						}
					  else{
						  cs = connection.prepareCall( "{call lessons_package.add_lesson(?,?)}");
						  System.out.println(id);
						  cs.setString(1, name.getText());
						   
						  Timestamp ts;
						  try{
							 ts = new Timestamp(Integer.parseInt(year.getText())-1900, Integer.parseInt(month.getText())-1, Integer.parseInt(day.getText()), Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()),Integer.parseInt(second.getText()), 0);
							 cs.setTimestamp(2, ts); 
							
						  }
						  catch(NumberFormatException e1){
							  //JOptionPane.showMessageDialog(null,e1.getMessage()); 
						  }
						  
			
						  
						  cs.execute();
						  JOptionPane.showMessageDialog(null, "You have added a new lesson");
						  View.updateTheLessonsView(statement,table, tableModel);
					  }
				 
				  } catch (SQLException ee) 
				  { // TODO Auto-generated catch block
				  //JOptionPane.showMessageDialog(null,ee.getMessage()); 
				  }
			}
				
			}		
		});
		contentPane.add(save);
		
		month = new JTextField();
		month.setBounds(158, 118, 163, 20);
		month.setColumns(10);
		contentPane.add(month);
		
		year = new JTextField();
		year.setBounds(158, 149, 163, 20);
		year.setColumns(10);
		contentPane.add(year);
		
		hour = new JTextField();
		hour.setBounds(158, 180, 163, 20);
		hour.setColumns(10);
		contentPane.add(hour);
		
		JLabel lblMonth = new JLabel("Month");
		lblMonth.setBounds(54, 120, 101, 14);
		contentPane.add(lblMonth);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(54, 152, 101, 14);
		contentPane.add(lblYear);
		
		JLabel lblTime = new JLabel("Hour");
		lblTime.setBounds(54, 183, 101, 14);
		contentPane.add(lblTime);
		
		lblMinute = new JLabel("minute");
		lblMinute.setBounds(54, 214, 101, 14);
		contentPane.add(lblMinute);
		
		lblSecond = new JLabel("Second");
		lblSecond.setBounds(54, 244, 101, 14);
		contentPane.add(lblSecond);
		
		second = new JTextField();
		second.setBounds(158, 241, 163, 20);
		second.setColumns(10);
		contentPane.add(second);
		
		minute = new JTextField();
		minute.setBounds(158, 211, 163, 20);
		minute.setColumns(10);
		contentPane.add(minute);
		
		
		
		
	}
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	public void setTheView(TableModel tableModelParent,JTable tableParent){
		 this.tableModel=tableModelParent;
		 this.table=tableParent;
		}
	public void setData(String name,Date d, BigDecimal idd){

		this.id=idd;
		this.name.setText(name);
		
		try{
			this.year.setText(Integer.toString(d.getYear()+1900));
		}
		catch(NullPointerException e1){
			this.year.setText("");
		}
		
		try{
			this.month.setText(Integer.toString(d.getMonth()+1));
		}
		catch(NullPointerException e1){
			this.month.setText("");
		}
		
		try{
			this.day.setText(Integer.toString(d.getDay()));
		}
		catch(NullPointerException e1){
			this.day.setText("");
		}
		
		try{
			this.hour.setText(Integer.toString(d.getHours()));
		}
		catch(NullPointerException e1){
			this.hour.setText("");
		}
		
		try{
			this.minute.setText(Integer.toString(d.getMinutes()));
		}
		catch(NullPointerException e1){
			this.minute.setText("");
		}
		
		try{
			this.second.setText(Integer.toString(d.getSeconds()));
		}
		catch(NullPointerException e1){
			this.second.setText("");
		}
		
		
		
	}
}

	

