package myFrames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import javax.swing.text.Caret;

import Helpfull.Month;
import Helpfull.View;
import oracle.sql.TIMESTAMP;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class StudentsExternalFrame extends JFrame {

	private JPanel contentPane;
	private JTextField name;
	private JTextField surname;
	private JTextField pesel;

	private Statement statement;
	private Connection connection;
	private JTable table;
	TableModel tableModel;
	private BigDecimal studentId;
	private JTextField place;
	private JTextField year;
	private JTextField behavior;
	private JLabel lblMinute;
	private JTextField desease;
	private JTextField month;
	private JTextField day;
	JComboBox passOrNot;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventsExternalFrame frame = new EventsExternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentsExternalFrame() {
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(244, 244, 244));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel jhtrjt = new JLabel("Name");
		jhtrjt.setBounds(54, 41, 101, 14);
		contentPane.add(jhtrjt);

		JLabel desdf = new JLabel("Surname");
		desdf.setBounds(54, 66, 101, 14);
		contentPane.add(desdf);

		JLabel lblNewLabel_2 = new JLabel("Pesel");
		lblNewLabel_2.setBounds(54, 98, 101, 14);
		contentPane.add(lblNewLabel_2);

		name = new JTextField();
		name.setBounds(158, 38, 163, 20);
		contentPane.add(name);
		name.setColumns(10);

		surname = new JTextField();
		surname.setBounds(158, 66, 163, 20);
		contentPane.add(surname);
		surname.setColumns(10);

		pesel = new JTextField();
		pesel.setBounds(158, 95, 163, 20);
		contentPane.add(pesel);
		pesel.setColumns(10);

		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (getTitle().equals("Edit")) {
					String c = passOrNot.getSelectedItem().toString();
					CallableStatement cs = null;
					try {
						if (year.getText().equals("") || place.getText().equals("") || pesel.getText().equals("")
								|| behavior.getText().equals("") || desease.getText().equals("")
								|| year.getText().equals("") || month.getText().equals("") || day.getText().equals("")
								|| name.getText().equals("") || surname.getText().equals("")||c.equals("")) {
							JOptionPane.showMessageDialog(null, "Fill all fields!");

						} else {

							cs = connection.prepareCall("{call students_package.edit_student(?,?,?,?,?,?,?,?,?)}");

							cs.setBigDecimal(1, studentId);
							cs.setString(2, name.getText());
							cs.setString(3, surname.getText());
							cs.setString(4, pesel.getText());
							String monthh = Month.getMonth(Integer.parseInt(month.getText()));
						    cs.setString(5,day.getText()+"-"+monthh+"-"+year.getText());
						    cs.setString(6, place.getText());
						    cs.setString(7, behavior.getText());
						    cs.setString(8, desease.getText());
						    cs.setString(9,c);
							cs.executeUpdate();
							JOptionPane.showMessageDialog(null, "You have eddited a new Student");
							View.updateTheStudentsView(statement, table, tableModel);
						}

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}

				}

				else // add
				{
					String c = passOrNot.getSelectedItem().toString();
					CallableStatement cs = null;
					try {
						if (year.getText().equals("") || place.getText().equals("") || pesel.getText().equals("")
								|| behavior.getText().equals("") || desease.getText().equals("")
								|| year.getText().equals("") || month.getText().equals("") || day.getText().equals("")
								|| name.getText().equals("") || surname.getText().equals("")||c.equals("")) {
							JOptionPane.showMessageDialog(null, "Fill all fields!");
						} else {
							cs = connection.prepareCall("{call students_package.add_student(?,?,?,?,?,?,?,?)}");

							
							cs.setString(1, name.getText());
							cs.setString(2, surname.getText());
							cs.setString(3, pesel.getText());
							Date date = new Date();
							
							String monthh = Month.getMonth(Integer.parseInt(month.getText()));
						    cs.setString(4,day.getText()+"-"+monthh+"-"+year.getText());
						    cs.setString(5, place.getText());
						    cs.setString(6, behavior.getText());
						    cs.setString(7, desease.getText());
						    cs.setString(8,c);
						   

							cs.executeUpdate();
							JOptionPane.showMessageDialog(null, "You have added a new Student");
							View.updateTheStudentsView(statement, table, tableModel);
						}

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}

				}
			}
		});

		save.setBounds(351, 116, 89, 23);
		contentPane.add(save);

		place = new JTextField();
		place.setColumns(10);
		place.setBounds(158, 220, 163, 20);
		contentPane.add(place);

		year = new JTextField();
		year.setColumns(10);
		year.setBounds(158, 126, 163, 20);
		contentPane.add(year);

		behavior = new JTextField();
		behavior.setColumns(10);
		behavior.setBounds(158, 251, 163, 20);
		contentPane.add(behavior);

		JLabel lblMonth = new JLabel("Place of birth");
		lblMonth.setBounds(54, 223, 101, 14);
		contentPane.add(lblMonth);

		JLabel lblYear = new JLabel("Year of birth");
		lblYear.setBounds(54, 133, 101, 14);
		contentPane.add(lblYear);

		JLabel lblTime = new JLabel("Behavior");
		lblTime.setBounds(54, 254, 101, 14);
		contentPane.add(lblTime);

		lblMinute = new JLabel("Type of desease");
		lblMinute.setBounds(54, 287, 101, 14);
		contentPane.add(lblMinute);

		desease = new JTextField();
		desease.setColumns(10);
		desease.setBounds(158, 287, 163, 20);
		contentPane.add(desease);

		month = new JTextField();
		month.setColumns(10);
		month.setBounds(158, 157, 163, 20);
		contentPane.add(month);

		day = new JTextField();
		day.setColumns(10);
		day.setBounds(158, 195, 163, 20);
		contentPane.add(day);

		JLabel lblDayOfBirth = new JLabel("Day of birth");
		lblDayOfBirth.setBounds(54, 198, 101, 14);
		contentPane.add(lblDayOfBirth);

		JLabel sdf = new JLabel("Month of birth");
		sdf.setBounds(54, 158, 101, 14);
		contentPane.add(sdf);
		
		JLabel label = new JLabel("Pass(yes or no)");
		label.setBounds(54, 323, 101, 14);
		contentPane.add(label);
		
		String []set={"","pass","not pass"};
		passOrNot = new JComboBox(set);
		passOrNot.setBounds(158, 322, 163, 20);
		contentPane.add(passOrNot);

	}

	public void setConnection(Connection connection, Statement statement) {
		this.connection = connection;
		this.statement = statement;

	}

	public void setTheView(TableModel tableModelParent, JTable tableParent) {
		this.tableModel = tableModelParent;
		this.table = tableParent;
	}

	public void setData(BigDecimal id, String name, String surname, BigDecimal pesel, Date date, String place,
			BigDecimal behavior, String desease,String k) {
		this.studentId = id;
		this.name.setText(name);
		this.surname.setText(surname);
		this.pesel.setText(pesel.toString());
		try {
			this.year.setText(Integer.toString(date.getYear()+1900));
		} catch (NullPointerException e1) {
			this.year.setText("");
		}
		try {
			this.month.setText(Integer.toString(date.getMonth()+1));
		} catch (NullPointerException e1) {
			this.month.setText("");
		}
		try {
			this.day.setText(Integer.toString(date.getDay()));
		} catch (NullPointerException e1) {
			this.day.setText("");
		}
		this.place.setText(place);
		this.behavior.setText(behavior.toString());
		this.desease.setText(desease);
		this.passOrNot.setSelectedItem(k);

	}
}
