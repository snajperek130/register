package myFrames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import net.proteanit.sql.DbUtils;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;

public class ParentsExternalFrames extends JFrame {

	private JPanel contentPane;
	private BigDecimal id;
	private JTextField surname;
	private JTextField name;
	private JTextField address;
	private JTextField phone;
	private JTextField pesel;
	private Statement statement;
	private Connection connection;

	private JTable table;
	TableModel tableModel;
	BigDecimal eventId=null;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ParentsExternalFrames frame = new ParentsExternalFrames();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ParentsExternalFrames() {

		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(244, 244, 244));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel nameLabel = new JLabel("name");
		nameLabel.setBounds(29, 75, 57, 14);
		contentPane.add(nameLabel);

		JLabel surnameLabel = new JLabel("Surname");
		surnameLabel.setBounds(284, 72, 57, 14);
		contentPane.add(surnameLabel);

		JLabel educationLabel = new JLabel("Phone");
		educationLabel.setBounds(29, 159, 76, 14);
		contentPane.add(educationLabel);

		JLabel trainingLabel = new JLabel("Address");
		trainingLabel.setBounds(284, 159, 112, 14);
		contentPane.add(trainingLabel);

		surname = new JTextField();
		surname.setBounds(351, 72, 176, 20);
		contentPane.add(surname);
		surname.setColumns(10);

		name = new JTextField();
		name.setBounds(79, 72, 176, 20);
		contentPane.add(name);
		name.setColumns(10);

		address = new JTextField();
		address.setColumns(10);
		address.setBounds(353, 156, 176, 20);
		contentPane.add(address);

		phone = new JTextField();
		phone.setColumns(10);
		phone.setBounds(79, 156, 176, 20);
		contentPane.add(phone);
		
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (getTitle().equals("Edit")) {

					CallableStatement cs = null;
					try {
						if (name.getText().equals("") || surname.getText().equals("") || phone.getText().equals("")
								|| address.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Fill all fields!");
						} else {
							
							cs = connection.prepareCall("{call parents_package.edit_parent(?,?,?,?,?,?)}");
							

							cs.setBigDecimal(1, id);
							cs.setString(2, name.getText());
							cs.setString(3, surname.getText());
							cs.setString(4, phone.getText());
							cs.setString(5, address.getText());
							cs.setString(6, pesel.getText());
							cs.execute();

							JOptionPane.showMessageDialog(null, "You have edited the Parent");
							View.updateTheParentsView(statement, table, tableModel);
						}

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}

				}
				
				
						

						
					

				else // add
				{
					
					CallableStatement cs = null;

					try {
						
							if (name.getText().equals("") || surname.getText().equals("")
									|| phone.getText().equals("") || address.getText().equals("")) {
								JOptionPane.showMessageDialog(null, "Fill all fields!");
							} else {
								cs = connection.prepareCall("{call parents_package.add_parent(?,?,?,?,?)}");
								cs.setString(1, name.getText());
								cs.setString(2, surname.getText());
								cs.setString(3, phone.getText());
								cs.setString(4, address.getText());
								cs.setString(5, pesel.getText());
								cs.executeUpdate();

								JOptionPane.showMessageDialog(null, "You have added a new Teacher");
								View.updateTheParentsView(statement, table, tableModel);
							}
						

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}
				}
			}
				
		});
			
		save.setBounds(252, 280, 89, 23);
		contentPane.add(save);
		
		pesel = new JTextField();
		pesel.setColumns(10);
		pesel.setBounds(203, 235, 176, 20);
		contentPane.add(pesel);
		
		JLabel lblPesel = new JLabel("Pesel");
		lblPesel.setBounds(168, 238, 76, 14);
		contentPane.add(lblPesel);
			
	}

	public void setConnection(Connection connection, Statement statement) {
		this.connection = connection;
		this.statement = statement;

	}

	public void setData(BigDecimal id, String n, String s, String e, String t) {
		this.id = id;
		this.name.setText(n);
		this.surname.setText(s);
		this.phone.setText(e);
		this.address.setText(t);

	}

	public void setTheView(TableModel tableModelParent, JTable tableParent) {
		this.tableModel = tableModelParent;
		this.table = tableParent;
	}
}
