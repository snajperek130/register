package myFrames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import net.proteanit.sql.DbUtils;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;

public class TeachersExternalFrame extends JFrame {

	private JPanel contentPane;
	private BigDecimal id;
	private JTextField surname;
	private JTextField name;
	private JTextField training;
	private JTextField education;

	private Statement statement;
	private Connection connection;

	private JTable table;
	TableModel tableModel;
	BigDecimal eventId=null;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeachersExternalFrame frame = new TeachersExternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TeachersExternalFrame() {

		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(244, 244, 244));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel nameLabel = new JLabel("name");
		nameLabel.setBounds(10, 96, 57, 14);
		contentPane.add(nameLabel);

		JLabel surnameLabel = new JLabel("Surname");
		surnameLabel.setBounds(252, 96, 57, 14);
		contentPane.add(surnameLabel);

		JLabel educationLabel = new JLabel("Education");
		educationLabel.setBounds(10, 180, 76, 14);
		contentPane.add(educationLabel);

		JLabel trainingLabel = new JLabel("Specjalized training");
		trainingLabel.setBounds(252, 180, 112, 14);
		contentPane.add(trainingLabel);

		surname = new JTextField();
		surname.setBounds(378, 93, 176, 20);
		contentPane.add(surname);
		surname.setColumns(10);

		name = new JTextField();
		name.setBounds(66, 93, 176, 20);
		contentPane.add(name);
		name.setColumns(10);

		training = new JTextField();
		training.setColumns(10);
		training.setBounds(378, 177, 176, 20);
		contentPane.add(training);

		education = new JTextField();
		education.setColumns(10);
		education.setBounds(66, 177, 176, 20);
		contentPane.add(education);
		
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (getTitle().equals("Edit")) {

					CallableStatement cs = null;
					try {
						if (name.getText().equals("") || surname.getText().equals("") || education.getText().equals("")
								|| training.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Fill all fields!");
						} else {
							
							cs = connection.prepareCall("{call teachers_package.edit_teacher(?,?,?,?,?)}");
							

							cs.setBigDecimal(1, id);
							cs.setString(2, name.getText());
							cs.setString(3, surname.getText());
							cs.setString(4, education.getText());
							cs.setString(5, training.getText());
							cs.execute();

							JOptionPane.showMessageDialog(null, "You have edited the Teacher");
							View.updateTheTachersView(statement, table, tableModel);
						}

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}

				}
				
				
						

						
					

				else // add
				{
					
					CallableStatement cs = null;

					try {
						
							if (name.getText().equals("") || surname.getText().equals("")
									|| education.getText().equals("") || training.getText().equals("")) {
								JOptionPane.showMessageDialog(null, "Fill all fields!");
							} else {
								cs = connection.prepareCall("{call teachers_package.add_teacher(?,?,?,?)}");
								cs.setString(1, name.getText());
								cs.setString(2, surname.getText());
								cs.setString(3, education.getText());
								cs.setString(4, training.getText());
								cs.execute();

								JOptionPane.showMessageDialog(null, "You have added a new Teacher");
								View.updateTheTachersView(statement, table, tableModel);
							}
						

					} catch (SQLException ee) { // TODO Auto-generated catch
												// block
						JOptionPane.showMessageDialog(null, ee.getMessage());
					}
				}
			}
				
		});
			
		save.setBounds(252, 280, 89, 23);
		contentPane.add(save);
			
	}

	public void setConnection(Connection connection, Statement statement) {
		this.connection = connection;
		this.statement = statement;

	}

	public void setData(BigDecimal id, String n, String s, String e, String t) {
		this.id = id;
		this.name.setText(n);
		this.surname.setText(s);
		this.education.setText(e);
		this.training.setText(t);

	}

	public void setTheView(TableModel tableModelParent, JTable tableParent) {
		this.tableModel = tableModelParent;
		this.table = tableParent;
	}
}
