package EventsTeachers;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;

public class EventsTeachersFirstFrame extends JFrame {

	private JPanel contentPane;

	
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	
	
	
	JButton addEvent;
	JButton removeEvent;
	
	BigDecimal eventId;
	
	String removeCommand="";
	String who="";
	String viewCommand="";
	
	String whoParent="";
	String removeCommandParent="";
	String setViewCommandParent="";
	String addCommandParent="";
	String setViewParentCommandParent="";

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventsTeachersFirstFrame frame = new EventsTeachersFirstFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EventsTeachersFirstFrame() {
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
getContentPane().setLayout(null);
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		scrollPane.setBounds(40, 35, 732, 274);
		getContentPane().add(scrollPane);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		
		addEvent = new JButton("Add");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(eventId);
				EventsTeachersExternalFrame eventsTeachersExternalFrame= new EventsTeachersExternalFrame();
				eventsTeachersExternalFrame.setTitle("Add");
				eventsTeachersExternalFrame.setId(eventId);
				eventsTeachersExternalFrame.setAddCommand(addCommandParent);
				eventsTeachersExternalFrame.setWho(who);
				eventsTeachersExternalFrame.setViewCommand(setViewCommandParent);
				eventsTeachersExternalFrame.setViewParentCommand(setViewParentCommandParent);
				eventsTeachersExternalFrame.setConnection(connection, statement);
				eventsTeachersExternalFrame.setTheView(tableModel, table);
				eventsTeachersExternalFrame.updateTheView();
				eventsTeachersExternalFrame.setVisible(true);
	
			}
		});
		addEvent.setBounds(180, 353, 89, 23);
		getContentPane().add(addEvent);
		
		removeEvent = new JButton("Remove");
		removeEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall(removeCommand);
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 2);
					  cs.setBigDecimal(1,id);
					  cs.setBigDecimal(2, eventId);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the "+who);
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a "+who+", please");
						}
					 
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		
					  
					  
					  
					  
				}
				updateTheView();
			}
		});
		removeEvent.setBounds(440, 353, 89, 23);
		getContentPane().add(removeEvent);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void setId(BigDecimal id) {
		this.eventId=id;
		
	}

	public void setWho(String who) {
		this.who=who;
	}
	public void setRemoveCommand(String remove) {
		this.removeCommand=remove;
	}
	public void setViewCommand(String view) {
		this.viewCommand=view;
	}
	public void setWhoParent(String who) {
		this.whoParent=who;
	}
	public void removeCommandParent(String remove) {
		this.removeCommandParent=remove;
	}
	public void setViewCommandParent(String view) {
		this.setViewCommandParent=view;
	}
	public void addCommandParent(String add) {
		this.addCommandParent=add;
	}
	public void setViewParentCommandParent(String parentView) {
		this.setViewParentCommandParent=parentView;
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery(viewCommand+eventId);
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
			
		}
	}
	
	}


