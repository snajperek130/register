package EventsTeachers;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import net.proteanit.sql.DbUtils;

public class EventsTeachersExternalFrame extends JFrame {
	

private JPanel contentPane;

	
	private JTable tableParent;
	TableModel tableModelParent;
	
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	
	String addCommand="";
	String viewCommand="";
	String parentViewCommand="";
	String who="";
	
	JButton addEvent;
	
	BigDecimal eventId;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EventsTeachersExternalFrame frame = new EventsTeachersExternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EventsTeachersExternalFrame() {
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
getContentPane().setLayout(null);
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		scrollPane.setBounds(10, 35, 762, 274);
		getContentPane().add(scrollPane);
		
		table= new JTable();
		scrollPane.setViewportView(table);
		
		
		addEvent = new JButton("Add");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CallableStatement cs = null;		
				  try { 
				  cs = connection.prepareCall(addCommand);
				  int selectedRowIndex = table.getSelectedRow();
				  int selectedColumnIndex = table.getSelectedColumn();
				  BigDecimal teacherId = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				 System.out.print(teacherId);
				 System.out.print(eventId);
				 
				  cs.setBigDecimal(1, teacherId);
				  cs.setBigDecimal(2, eventId);
				  cs.executeUpdate();
				  JOptionPane.showMessageDialog(null, "You have added the "+who+"");
				  updateParentView(statement,tableParent, tableModelParent);
				  } catch (SQLException ee) 
				  { // TODO Auto-generated catch block
				  JOptionPane.showMessageDialog(null,ee.getMessage()); 
				  }
				  catch (ArrayIndexOutOfBoundsException e1){
					  JOptionPane.showConfirmDialog(null, "First select a "+who+", please");
					}
				 
				  try {
					cs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
	
			}
		});
		addEvent.setBounds(334, 356, 89, 23);
		getContentPane().add(addEvent);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery(viewCommand);
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
			
		}
	}
	

	public void setId(BigDecimal id) {
		this.eventId=id;
		
	}
	
	public void setWho(String who){
		this.who=who;
	}
	public void setAddCommand(String add){
		this.addCommand=add;
	}
	public void setViewCommand(String view){
		this.viewCommand=view;
	}
	public void setViewParentCommand(String parentView){
		this.parentViewCommand=parentView;
	}
	public void setTheView(TableModel tableModelParent, JTable tableParent) {
		this.tableModelParent = tableModelParent;
		this.tableParent = tableParent;
		
	}
	public void updateParentView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery(parentViewCommand+eventId);
			tableModel = DbUtils.resultSetToTableModel(rs);
			tableParent.setModel(tableModel);
		 	rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
			
		}
}

}
