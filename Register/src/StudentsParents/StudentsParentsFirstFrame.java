package StudentsParents;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;

public class StudentsParentsFirstFrame extends JFrame {

	private JPanel contentPane;

	
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	
	
	
	JButton addEvent;
	JButton removeEvent;
	
	BigDecimal studentId;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentsParentsFirstFrame frame = new StudentsParentsFirstFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentsParentsFirstFrame() {
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
getContentPane().setLayout(null);
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		scrollPane.setBounds(40, 35, 732, 274);
		getContentPane().add(scrollPane);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		
		addEvent = new JButton("Add");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(studentId);
				StudentsParentsExternalFrame studentsParentsExternalFrame= new StudentsParentsExternalFrame();
				studentsParentsExternalFrame.setTitle("Add");
				studentsParentsExternalFrame.setId(studentId);
				studentsParentsExternalFrame.setConnection(connection, statement);
				studentsParentsExternalFrame.setTheView(tableModel, table);
				studentsParentsExternalFrame.updateTheView();
				studentsParentsExternalFrame.setVisible(true);
	
			}
		});
		addEvent.setBounds(180, 353, 89, 23);
		getContentPane().add(addEvent);
		
		removeEvent = new JButton("Remove");
		removeEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
						  cs = connection.prepareCall("{call stu_par_package.delete_parent_from_student(?,?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal idParent;
					  idParent = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 2);
					 
						  cs.setBigDecimal(1, idParent);
						  cs.setBigDecimal(2, studentId);
						  cs.execute();
						  JOptionPane.showMessageDialog(null, "You have deleted the Parent");
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a parent, please");
						}
					 
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					  
					  
					  
					  
				}
				updateTheView();
			}
		});
		removeEvent.setBounds(440, 353, 89, 23);
		getContentPane().add(removeEvent);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void setId(BigDecimal id) {
		this.studentId=id;
		
	}
	public void updateTheView(){
		try
		{
		
			ResultSet rs = statement.executeQuery("SELECT s.student_id,s.student_name,pp.parent_id,pp.parent_name,pp.surname,pp.phone,pp.address,pp.pesel FROM par_stu a, students s, parents pp where pp.parent_id=a.parents_ID_parent and s.student_ID=a.students_ID_student and a.students_ID_student="+studentId);
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
			
		}
	}
	
	}


