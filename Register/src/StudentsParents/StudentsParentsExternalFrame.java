package StudentsParents;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import Helpfull.View;
import net.proteanit.sql.DbUtils;

public class StudentsParentsExternalFrame extends JFrame {
	

private JPanel contentPane;

	
	private JTable tableParent;
	TableModel tableModelParent;
	
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	

	
	JButton addEvent;
	
	BigDecimal studentId;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentsParentsExternalFrame frame = new StudentsParentsExternalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentsParentsExternalFrame() {
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
getContentPane().setLayout(null);
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		scrollPane.setBounds(10, 35, 762, 274);
		getContentPane().add(scrollPane);
		
		table= new JTable();
		scrollPane.setViewportView(table);
		
		
		addEvent = new JButton("Add");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CallableStatement cs = null;		
				  try { 
				  cs = connection.prepareCall("{call stu_par_package.add_parent_to_student(?,?)}");
				  int selectedRowIndex = table.getSelectedRow();
				  int selectedColumnIndex = table.getSelectedColumn();
				  BigDecimal parentId = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				  cs.setBigDecimal(1, parentId);
				  cs.setBigDecimal(2, studentId);
				  cs.executeUpdate();
				  JOptionPane.showMessageDialog(null, "You have added the parent");
				  updateParentView(statement,tableParent, tableModelParent);
				  } catch (SQLException ee) 
				  { // TODO Auto-generated catch block
				  JOptionPane.showMessageDialog(null,ee.getMessage()); 
				  }
				  catch (ArrayIndexOutOfBoundsException e1){
					  JOptionPane.showConfirmDialog(null, "First select a student, please");
					}
				 
				  try {
					cs.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
	
			}
		});
		addEvent.setBounds(334, 356, 89, 23);
		getContentPane().add(addEvent);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT *FROM parents");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
			
		}
	}
	

	public void setId(BigDecimal id) {
		this.studentId=id;
		
	}
	public void setTheView(TableModel tableModelParent, JTable tableParent) {
		this.tableModelParent = tableModelParent;
		this.tableParent = tableParent;
		
	}
	public void updateParentView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT s.student_id,s.student_name,pp.parent_id,pp.parent_name,pp.surname,pp.phone,pp.address,pp.pesel FROM par_stu a, students s, parents pp where pp.parent_id=a.parents_ID_parent and s.student_ID=a.students_ID_student and a.students_ID_student="+studentId);
			tableModel = DbUtils.resultSetToTableModel(rs);
			tableParent.setModel(tableModel);
		 	rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsStudentsFirstFrame problem");
			
		}
}

}
