

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import myPanels.Cal;
import myPanels.EventsFirstPanel;
import myPanels.LessonsFirstPanel;
import myPanels.ParentsFirstPanel;
import myPanels.StudentsFirstPanel;
import myPanels.TeachersFirstPanel;
import myPanels.Home;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class MenuFrame extends JFrame {

	private JPanel contentPane;
	JMenuBar menuWithMainButtons;
	JPanel left;
	private JTable table;
	private CardLayout c; 
	private Home home;
	private Statement statement=null;
    private Connection connection;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuFrame frame = new MenuFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public MenuFrame() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		setPreferredSize(new Dimension(800, 500));
		menuWithMainButtons = new JMenuBar();
		setJMenuBar(menuWithMainButtons);
		c=new CardLayout();
		contentPane=new JPanel();
		getContentPane().add(contentPane);
		contentPane.setLayout(c);
	
		
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","KONRAD","olejek1");
			statement = connection.createStatement();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "MenuFrame connection problem");
		}
		
		
		JButton buttonHome = new JButton("Home");
		buttonHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				home= new Home();
				contentPane.add(home,"home");
				c.show(contentPane, "home");
				pack();
			
			}
		});
		menuWithMainButtons.add(buttonHome);
		
		
		JButton MenuButtonLessons = new JButton("Lessons");
		MenuButtonLessons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LessonsFirstPanel firstLessonsPanel=new LessonsFirstPanel ();
				firstLessonsPanel.setConnection(connection,statement);
				firstLessonsPanel.updateTheView();
				contentPane.add(firstLessonsPanel,"firstLessonsPanel");
				c.show(contentPane, "firstLessonsPanel");
				pack();
			}
		});
		menuWithMainButtons.add(MenuButtonLessons);
		
		
		JButton MenuButtonStudents = new JButton("Students");
		MenuButtonStudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentsFirstPanel studentsFirstPanel=new StudentsFirstPanel();
				studentsFirstPanel.setConnection(connection,statement);
				studentsFirstPanel.updateTheView();
				contentPane.add(studentsFirstPanel,"studentsFirstPanel");
				c.show(contentPane, "studentsFirstPanel");
				pack();
				
			}
		});
		menuWithMainButtons.add(MenuButtonStudents);
		
		JButton MenuButtonEvents = new JButton("Events");
		MenuButtonEvents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventsFirstPanel eventsFirstPanel=new EventsFirstPanel();
				eventsFirstPanel.setConnection(connection,statement);
				eventsFirstPanel.updateTheView();
				contentPane.add(eventsFirstPanel,"eventsFirstPanel");
				c.show(contentPane, "eventsFirstPanel");
				pack();
			}
		});
		menuWithMainButtons.add(MenuButtonEvents);
		
		JButton MenuButtonTeachers = new JButton("Teachers");
		MenuButtonTeachers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeachersFirstPanel teachersFirstPanel=new TeachersFirstPanel();
			 	teachersFirstPanel.setConnection(connection,statement);
			 	teachersFirstPanel.updateTheView();
				contentPane.add(teachersFirstPanel,"teachersFirstPanel");
				c.show(contentPane, "teachersFirstPanel");
				pack();
				
			}
		});
		menuWithMainButtons.add(MenuButtonTeachers);
		
		JButton MenuButtonParents = new JButton("Parents");
		MenuButtonParents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ParentsFirstPanel parentsFirstPanel=new ParentsFirstPanel();
			 	parentsFirstPanel.setConnection(connection,statement);
			 	parentsFirstPanel.updateTheView();
				contentPane.add(parentsFirstPanel,"parentsFirstPanel");
				c.show(contentPane, "parentsFirstPanel");
				pack();
			}
		});
		menuWithMainButtons.add(MenuButtonParents);
		
		

		
	}
	
}
