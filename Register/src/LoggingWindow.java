

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.event.AncestorListener;

import Helpfull.View;

import java.sql.*;
import myPanels.Cal;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.KeyStroke;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.DropMode;
import javax.swing.JPasswordField;

public class LoggingWindow {

	private JFrame frame;
	private JTextField p;
	private JTextField l;
	private JTextField textField;
	private JTextField usernameField;
	private JPasswordField passwordField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoggingWindow window = new LoggingWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoggingWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 696, 421);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		// window to center
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y);
		//
		
		JLabel labelUsername = new JLabel("Username:");
		labelUsername.setHorizontalAlignment(SwingConstants.CENTER);
		labelUsername.setFont(new Font("Tahoma", Font.BOLD, 20));
		labelUsername.setBounds(20, 95, 205, 35);
		labelUsername.addAncestorListener((AncestorListener) l);
		frame.getContentPane().add(labelUsername);
		
		
		JLabel labelPassword = new JLabel("Password:");
		labelPassword.setHorizontalAlignment(SwingConstants.CENTER);
		labelPassword.setFont(new Font("Tahoma", Font.BOLD, 20));
		labelPassword.setBounds(20, 160, 205, 35);
		frame.getContentPane().add(labelPassword);
		
		// button login
		JButton buttonLogin = new JButton("Login");
		buttonLogin.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent arg0) {
				
				String login = usernameField.getText();
				String password = passwordField.getText();
				
				try
				{
					//Class.forName("oracle.jdbc.OracleDriver");
					Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",login,password);
					JOptionPane.showMessageDialog(null, "You have logged correctly");
					frame.dispose();
					con.close();
					MenuFrame menu = new MenuFrame();
					menu.setVisible(true);
					
				}
				catch(Exception ex)
				{
					JOptionPane.showMessageDialog(null, "Incorrect loggin or password.");
				}
			
			}
		});

		
		View.doClick(frame, buttonLogin);

	
		buttonLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonLogin.setBounds(196, 266, 89, 23);
		frame.getContentPane().add(buttonLogin);
		
		//button cancel
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Container c = buttonCancel.getParent();
				do 
	                c = c.getParent(); 
	            while (!(frame instanceof JFrame));                                      
	            ((JFrame) frame).dispose();
			}
		});
		buttonCancel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonCancel.setBounds(387, 266, 89, 23);
		frame.getContentPane().add(buttonCancel);
		//
		
		textField = new JTextField();
		textField.setBounds(483, 106, -189, -36);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		usernameField = new JTextField();
		usernameField.setHorizontalAlignment(SwingConstants.CENTER);
		usernameField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		usernameField.setBounds(235, 95, 205, 35);
		frame.getContentPane().add(usernameField);
		usernameField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setEchoChar('*');
		passwordField.setBounds(235, 164, 205, 35);
		frame.getContentPane().add(passwordField);
	}
}
