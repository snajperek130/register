package myPanels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Home extends JPanel {

	
	
	public Home() {
		
		
		setPreferredSize(new Dimension(800, 500));
		setLayout(null);
		JLabel labelImage = new JLabel("");
		labelImage.setBounds(76, 73, 256, 256);
		labelImage.setHorizontalAlignment(SwingConstants.LEFT);
		add(labelImage);
		labelImage.setVerticalAlignment(SwingConstants.TOP);
		Image menuRegister=new ImageIcon(this.getClass().getResource("/register.png")).getImage();
		labelImage.setIcon(new ImageIcon(menuRegister));
		
		JLabel label = new JLabel("Register");
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBounds(376, 37, 106, 31);
		label.setFont(new Font("Tahoma", Font.BOLD, 25));
		add(label);
	}

}
