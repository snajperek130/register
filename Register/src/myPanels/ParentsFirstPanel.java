package myPanels;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import Helpfull.View;
import myFrames.ParentsExternalFrames;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

public class ParentsFirstPanel extends JPanel {
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	
	
	JButton addEvent;
	JButton removeEvent;
	private JButton editEvent;
	

	public ParentsFirstPanel() {
		setPreferredSize(new Dimension(800, 500));
		setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		scrollPane.setBounds(40, 35, 732, 274);
		add(scrollPane);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		
		addEvent = new JButton("Add");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ParentsExternalFrames parentExternalFrameAdd= new ParentsExternalFrames();
				parentExternalFrameAdd.setTitle("Add");
				parentExternalFrameAdd.setConnection(connection, statement);
				parentExternalFrameAdd.setTheView(tableModel, table);
				parentExternalFrameAdd.setVisible(true);
	
			}
		});
		addEvent.setBounds(49, 353, 89, 23);
		add(addEvent);
		
		removeEvent = new JButton("Remove");
		removeEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall("{call parents_package.delete_parent(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  
					  View.updateTheParentsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall("{call stu_par_package.delete_parent(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the Parent");
					  View.updateTheParentsView(statement,table, tableModel);
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a parent, please");
						}
					 
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
		
				}
		
					

				
				}
		
			
		});

		removeEvent.setBounds(633, 353, 89, 23);
		add(removeEvent);
		
		editEvent = new JButton("Edit");
		editEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				ParentsExternalFrames edit= new ParentsExternalFrames();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				String name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				String surname =(String) table.getModel().getValueAt(selectedRowIndex, 2);
				String education =(String) table.getModel().getValueAt(selectedRowIndex, 3);
				String training =(String) table.getModel().getValueAt(selectedRowIndex, 4);
				edit.setData(id,name, surname, education, training);	
				edit.setTheView(tableModel, table);
				edit.setConnection(connection, statement);
				edit.setVisible(true);
				
				}
				catch (ArrayIndexOutOfBoundsException e1){
					
				}
			}
		});
		editEvent.setBounds(339, 353, 89, 23);
		add(editEvent);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM parents order by parent_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "ParentFirstPanel problem");
			
		}
	}
	


	
	
}
