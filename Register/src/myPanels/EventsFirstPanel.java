package myPanels;

import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import EventsStudents.EventsStudentsFirstFrame;
import EventsTeachers.EventsTeachersFirstFrame;
import Helpfull.View;
import myFrames.EventsExternalFrame;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;
import oracle.sql.TIMESTAMP;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class EventsFirstPanel extends JPanel {
	private JTable table;

	private Statement statement;
	private Connection connection;
	
	TableModel tableModel;
	/**
	 * Create the panel.
	 */
	public EventsFirstPanel() {
		setPreferredSize(new Dimension(900, 500));
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 24, 547, 213);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton showStudents = new JButton("Show students");
		showStudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventsStudentsFirstFrame showStudents = new EventsStudentsFirstFrame();
				showStudents.setTitle("Add");
				showStudents.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				if(id==null){
					
				}
				else{
					showStudents.setId(id);
					showStudents.setWho("student");
					showStudents.setRemoveCommand("{call eve_stu_package.delete_student_from_event(?,?)}");
					showStudents.setViewCommand("SELECT e.event_id,e.name,s.student_id,s.student_name,s.surname,s.pesel,s.date_of_birth,s.place_of_birth,s.behavior, s.type_of_disease FROM eve_stu a, students s, events e where e.EVENT_ID=a.EVENTS_ID_EVENT and s.student_ID=a.students_ID_student and a.EVENTS_ID_EVENT= ");
					showStudents.addCommandParent("{call eve_stu_package.add_student_to_event(?,?)}");
					showStudents.setViewCommandParent("SELECT *FROM students");
					showStudents.setViewParentCommandParent("SELECT e.event_id,e.name,s.student_id,s.student_name,s.surname,s.pesel,s.date_of_birth,s.place_of_birth,s.behavior, s.type_of_disease FROM eve_stu a, students s, events e where e.EVENT_ID=a.EVENTS_ID_EVENT and s.student_ID=a.students_ID_student and a.EVENTS_ID_EVENT= ");
					showStudents.updateTheView();
					showStudents.setVisible(true);
					
				}
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				
			}
		});
		showStudents.setBounds(88, 296, 128, 23);
		add(showStudents);

		JButton addEvent = new JButton("Add event");
		addEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventsExternalFrame eventsExternalFrameAdd= new EventsExternalFrame();
				eventsExternalFrameAdd.setTitle("Add");
				eventsExternalFrameAdd.setConnection(connection, statement);
				
				eventsExternalFrameAdd.setTheView(tableModel, table);
				eventsExternalFrameAdd.setVisible(true);
			}
		});
		addEvent.setBounds(582, 21, 116, 23);
		add(addEvent);
		
		JButton editEvent = new JButton("Edit event");
		editEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) throws ArrayIndexOutOfBoundsException {
				//Timestamp t = new Timestamp(eventMask);
				Timestamp time=null;
				
				//try
				//{
				EventsExternalFrame edit= new EventsExternalFrame();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id=null;
				String name="";
				BigDecimal number_of_people=null;
				TIMESTAMP t;
				
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				number_of_people = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 2);
				t = new TIMESTAMP();
				t= (TIMESTAMP) table.getModel().getValueAt(selectedRowIndex, 3);
				edit.setConnection(connection, statement);
				edit.setTheView(tableModel, table);
				edit.setVisible(true);
				
				    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
				    Date parsedDate=null;
				    try {
					try {
						parsedDate = (Date) dateFormat.parse(t.stringValue());
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				    time = new java.sql.Timestamp(parsedDate.getTime());
				    System.out.println(id);
				   
				    }catch(NullPointerException a){
						
					}
				edit.setData( number_of_people, name, time,id);
				
			}
			
			
		});
		editEvent.setBounds(582, 76, 116, 23);
		add(editEvent);
		
		JButton showTeachers = new JButton("Show teachers");
		showTeachers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventsTeachersFirstFrame showTeachers = new EventsTeachersFirstFrame();
				showTeachers.setTitle("Add");
				showTeachers.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				if(id==null){
					
				}
				else{
					showTeachers.setId(id);
					showTeachers.setWho("teacher");
					showTeachers.setRemoveCommand("{call tea_eve_package.delete_teacher_from_event(?,?)}");
					showTeachers.setViewCommand("SELECT e.event_id,e.name,t.teacher_id,t.name,t.surname,t.education,t.SPECIALIZED_TRAINING FROM tea_eve a, teachers t, events e where e.event_ID=a.events_ID_event and t.TEACHER_ID=a.TEACHERS_ID_TEACHER and a.events_ID_event=");
					showTeachers.addCommandParent("{call tea_eve_package.add_teacher_to_event(?,?)}");
					showTeachers.setViewCommandParent("SELECT *FROM teachers");
					showTeachers.setViewParentCommandParent("SELECT e.event_id,e.name,t.teacher_id,t.name,t.surname,t.education,t.SPECIALIZED_TRAINING FROM tea_eve a, teachers t, events e where e.event_ID=a.events_ID_event and t.TEACHER_ID=a.TEACHERS_ID_TEACHER and a.events_ID_event=");
					showTeachers.updateTheView();
					showTeachers.setVisible(true);
					
				}
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				
				
				
			 	
			 
			}
		});
		showTeachers.setBounds(297, 296, 128, 23);
		add(showTeachers);
		
		JButton deleteEvent = new JButton("Delete Event");
		deleteEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall( "{call events_package.delete_event(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the Event");
					  updateTheView();
					  View.updateTheEventsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call eve_stu_package.delete_event(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  updateTheView();
					  View.updateTheEventsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call tea_eve_package.delete_event(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  updateTheView();
					  View.updateTheEventsView(statement,table, tableModel);
					  
					  
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a event, please");
						}
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		
				}
			}
		});
		deleteEvent.setBounds(582, 130, 116, 23);
		add(deleteEvent);
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM events order by event_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "EventsFirstPanel problem");
			
		}
	}
	
}
