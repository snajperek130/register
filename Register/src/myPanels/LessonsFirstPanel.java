package myPanels;

import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import EventsStudents.EventsStudentsFirstFrame;
import EventsTeachers.EventsTeachersFirstFrame;
import Helpfull.View;
import myFrames.EventsExternalFrame;
import myFrames.LessonsExternalFrame;
import myFrames.StudentsExternalFrame;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;
import oracle.sql.TIMESTAMP;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class LessonsFirstPanel extends JPanel {
	private JTable table;

	private Statement statement;
	private Connection connection;
	
	
	
	TableModel tableModel;
	/**
	 * Create the panel.
	 */
	public LessonsFirstPanel() {
		setPreferredSize(new Dimension(900, 500));
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 24, 547, 213);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);

		JButton addLesson = new JButton("Add lesson");
		addLesson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LessonsExternalFrame lessonsExternalFrameAdd= new LessonsExternalFrame();
				lessonsExternalFrameAdd.setTitle("Add");
				lessonsExternalFrameAdd.setConnection(connection, statement);	
				lessonsExternalFrameAdd.setTheView(tableModel, table);
				lessonsExternalFrameAdd.setVisible(true);
			}
		});
		addLesson.setBounds(582, 21, 116, 23);
		add(addLesson);
		
		JButton editLesson = new JButton("Edit lesson");
		editLesson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) throws ArrayIndexOutOfBoundsException {
				Timestamp time=null;
			
				LessonsExternalFrame edit= new LessonsExternalFrame();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id=null;
				String name="";
				
				TIMESTAMP t;
				
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex,0);
				
				name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				t = new TIMESTAMP();
				t= (TIMESTAMP) table.getModel().getValueAt(selectedRowIndex, 2);
				edit.setConnection(connection, statement);
				edit.setTheView(tableModel, table);
				edit.setVisible(true);
			
				    
				
				    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
				    Date parsedDate=null;
				    try {
						try {
							parsedDate = (Date) dateFormat.parse(t.stringValue());
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							//e1.printStackTrace();
						}
					    time = new java.sql.Timestamp(parsedDate.getTime());
					
					   
					    }catch(NullPointerException a){
							
						}
				edit.setData( name, time, id);
				
			}
			
			
		});
		
				
			
					
				

		/*
		StudentsExternalFrame edit= new StudentsExternalFrame();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal studentId=null;
				String name="";
				String surname="";
				BigDecimal pesel=null;
				String place;
				BigDecimal behavior=null;
				String type="";
				TIMESTAMP t;
				try{
				studentId = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				surname =(String) table.getModel().getValueAt(selectedRowIndex, 2);
				pesel = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 3);
				Date date = new Date();
				date= (Date) table.getModel().getValueAt(selectedRowIndex, 4);
				place =(String) table.getModel().getValueAt(selectedRowIndex, 5);
				behavior = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 6);
				type =(String) table.getModel().getValueAt(selectedRowIndex, 7);
				
				edit.setConnection(connection, statement);
				edit.setTheView(tableModel, table);
				edit.setVisible(true);
				try{
				   
				   edit.setData(studentId, name,surname,pesel,date,place,behavior,type);
				
				    
				}
				
			catch(Exception e1){//this generic but you can control another types of exception
				e1.getMessage();
			}
				}catch(ArrayIndexOutOfBoundsException ee){
					JOptionPane.showMessageDialog(null, "First select a student");
				}
				
				
				
				
			}
			
			
		});
		*/
		editLesson.setBounds(582, 76, 116, 23);
		add(editLesson);
		
		JButton deleteLesson = new JButton("Delete Lesson");
		deleteLesson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall( "{call lessons_package.delete_lesson(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the lesson");
					  updateTheView();
					  View.updateTheLessonsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call les_stu_package.delete_lesson(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  updateTheView();
					  View.updateTheLessonsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call tea_les_package.delete_lesson(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  updateTheView();
					  View.updateTheLessonsView(statement,table, tableModel);
					 
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  //JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a lesson, please");
						}
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						//e1.printStackTrace();
					}
		
				}
			}
		});
		deleteLesson.setBounds(582, 130, 116, 23);
		add(deleteLesson);
		
		JButton showTeachers = new JButton("Show teachers");
		showTeachers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventsTeachersFirstFrame showTeachers = new EventsTeachersFirstFrame();
				showTeachers.setTitle("Add");
				showTeachers.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id=null;
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e1){
					JOptionPane.showMessageDialog(null, "First select an lesson!");
				}
				if(id==null){
					
				}
				else{
					showTeachers.setId(id);
					showTeachers.setWho("teacher");
					showTeachers.setRemoveCommand("{call tea_les_package.delete_teacher_from_lesson(?,?)}");
					showTeachers.setViewCommand("SELECT l.lesson_id,l.lesson_name,t.teacher_id,t.name,t.surname,t.education,t.SPECIALIZED_TRAINING FROM tea_les a, teachers t, lessons l where l.lesson_ID=a.lessons_ID_lesson and t.TEACHER_ID=a.TEACHERS_ID_TEACHER and a.lessons_ID_lesson=");
					showTeachers.addCommandParent("{call tea_les_package.add_teacher_to_lesson(?,?)}");
					showTeachers.setViewCommandParent("SELECT *FROM teachers");
					showTeachers.setViewParentCommandParent("SELECT l.lesson_id,l.lesson_name,t.teacher_id,t.name,t.surname,t.education,t.SPECIALIZED_TRAINING FROM tea_les a, teachers t, lessons l where l.lesson_ID=a.lessons_ID_lesson and t.TEACHER_ID=a.TEACHERS_ID_TEACHER and a.lessons_ID_lesson=");
					
					showTeachers.updateTheView();
					showTeachers.setVisible(true);
					
				}
				}catch(ArrayIndexOutOfBoundsException e1){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				
				
				
			 	
			 
			}
		});
		showTeachers.setBounds(329, 293, 128, 23);
		add(showTeachers);
		
		JButton showStudents = new JButton("Show students");
		showStudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventsStudentsFirstFrame showStudents = new EventsStudentsFirstFrame();
				showStudents.setTitle("Add");
				showStudents.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				if(id==null){
					
				}
				else{
					showStudents.setId(id);
					showStudents.setWho("student");
					showStudents.setRemoveCommand("{call les_stu_package.delete_student_from_lesson(?,?)}");
					showStudents.setViewCommand("SELECT l.lesson_id,l.lesson_name,s.student_id,s.surname,s.pesel,s.date_of_birth,s.place_of_birth,s.behavior,s.type_of_disease FROM les_stu a, students s, lessons l where l.lesson_ID=a.lessons_ID_lesson and s.student_ID=a.students_ID_student and a.lessons_ID_lesson=");
					showStudents.addCommandParent("{call les_stu_package.add_student_to_lesson(?,?)}");
					showStudents.setViewCommandParent("SELECT *FROM students");
					showStudents.setViewParentCommandParent("SELECT l.lesson_id,l.lesson_name,s.student_id,s.surname,s.pesel,s.date_of_birth,s.place_of_birth,s.behavior,s.type_of_disease FROM les_stu a, students s, lessons l where l.lesson_ID=a.lessons_ID_lesson and s.student_ID=a.students_ID_student and a.lessons_ID_lesson=");
					
					showStudents.updateTheView();
					showStudents.setVisible(true);
					
				}
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				
			}
		});
			
		showStudents.setBounds(120, 293, 128, 23);
		add(showStudents);
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT lesson_id, lesson_name,lesson_date from lessons");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "LessonsFirstPanel problem");
			
		}
	}
	
}
