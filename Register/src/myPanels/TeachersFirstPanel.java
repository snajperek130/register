package myPanels;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import Helpfull.View;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

public class TeachersFirstPanel extends JPanel {
	private JTable table;
	TableModel tableModel;

	private Statement statement;
	private Connection connection;
	
	
	JButton addTeacher;
	JButton removeTeacher;
	private JButton editTeacher;
	

	public TeachersFirstPanel() {
		setPreferredSize(new Dimension(800, 500));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 35, 732, 274);
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		setLayout(null);
		add(scrollPane);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		
		addTeacher = new JButton("Add");
		addTeacher.setBounds(318, 371, 140, 23);
		addTeacher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeachersExternalFrame teachersExternalFrameAdd= new TeachersExternalFrame();
				teachersExternalFrameAdd.setTitle("Add");
				teachersExternalFrameAdd.setConnection(connection, statement);
				teachersExternalFrameAdd.setTheView(tableModel, table);
				teachersExternalFrameAdd.setVisible(true);
	
			}
		});
		add(addTeacher);
		
		removeTeacher = new JButton("Remove");
		removeTeacher.setBounds(583, 371, 140, 23);
		removeTeacher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall("{call teachers_package.delete_teacher(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  View.updateTheTachersView(statement,table, tableModel);
					  
					  cs = connection.prepareCall("{call tea_eve_package.delete_teacher(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  View.updateTheTachersView(statement,table, tableModel);
					  
					  cs = connection.prepareCall("{call tea_les_package.delete_teacher(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the Teacher");
					  View.updateTheTachersView(statement,table, tableModel);
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a teacher, please");
						}
					 
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
		
				}
		
					

				
				}
		
			
		});
		add(removeTeacher);
		
		editTeacher = new JButton("Edit");
		editTeacher.setBounds(74, 371, 140, 23);
		editTeacher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				TeachersExternalFrame edit= new TeachersExternalFrame();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				String name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				String surname =(String) table.getModel().getValueAt(selectedRowIndex, 2);
				String education =(String) table.getModel().getValueAt(selectedRowIndex, 3);
				String training =(String) table.getModel().getValueAt(selectedRowIndex, 4);
				edit.setData(id,name, surname, education, training);	
				edit.setTheView(tableModel, table);
				edit.setConnection(connection, statement);
				edit.setVisible(true);
				
				}
				catch (ArrayIndexOutOfBoundsException e1){
					
				}
			}
		});
		add(editTeacher);
		
		
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM teachers order by teacher_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "TeacherFirstPanel problem");
			
		}
	}
}
