package myPanels;

import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import EventsStudents.EventsStudentsFirstFrame;
import EventsTeachers.EventsTeachersFirstFrame;
import Helpfull.View;
import StudentsParents.StudentsParentsFirstFrame;
import myFrames.EventsExternalFrame;
import myFrames.StudentsExternalFrame;
import myFrames.TeachersExternalFrame;
import net.proteanit.sql.DbUtils;
import oracle.sql.TIMESTAMP;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class StudentsFirstPanel extends JPanel {
	private JTable table;

	private Statement statement;
	private Connection connection;
	
	TableModel tableModel;
	/**
	 * Create the panel.
	 */
	public StudentsFirstPanel() {
		setPreferredSize(new Dimension(900, 500));
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 24, 547, 213);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);

		JButton addStudent = new JButton("Add Student");
		addStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				EventsExternalFrame eventsExternalFrameAdd= new EventsExternalFrame();
				eventsExternalFrameAdd.setTitle("Add");
				eventsExternalFrameAdd.setConnection(connection, statement);
				
				eventsExternalFrameAdd.setTheView(tableModel, table);
				eventsExternalFrameAdd.setVisible(true);
				*/
				
				StudentsExternalFrame studentExternalFrameAdd = new StudentsExternalFrame();
				studentExternalFrameAdd.setTitle("Add");
				studentExternalFrameAdd.setConnection(connection, statement);
				
				studentExternalFrameAdd.setTheView(tableModel, table);
				studentExternalFrameAdd.setVisible(true);
			}
		});
		addStudent.setBounds(582, 21, 116, 23);
		add(addStudent);
		
		JButton editStudent = new JButton("Edit Student");
		editStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) throws ArrayIndexOutOfBoundsException {
				
				StudentsExternalFrame edit= new StudentsExternalFrame();
				edit.setTitle("Edit");
				
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal studentId=null;
				String name="";
				String surname="";
				BigDecimal pesel=null;
				String place;
				BigDecimal behavior=null;
				String type="";
				TIMESTAMP t;
				try{
				studentId = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				name =(String) table.getModel().getValueAt(selectedRowIndex, 1);
				surname =(String) table.getModel().getValueAt(selectedRowIndex, 2);
				pesel = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 3);
				Date date = new Date();
				date= (Date) table.getModel().getValueAt(selectedRowIndex, 4);
				place =(String) table.getModel().getValueAt(selectedRowIndex, 5);
				behavior = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 6);
				type =(String) table.getModel().getValueAt(selectedRowIndex, 7);
				String k=  (String) table.getModel().getValueAt(selectedRowIndex, 8);
				edit.setConnection(connection, statement);
				edit.setTheView(tableModel, table);
				edit.setVisible(true);
				try{
				   
				   edit.setData(studentId, name,surname,pesel,date,place,behavior,type,k);
				
				    
				}
				
			catch(Exception e1){//this generic but you can control another types of exception
				e1.getMessage();
			}
				}catch(ArrayIndexOutOfBoundsException ee){
					JOptionPane.showMessageDialog(null, "First select a student");
				}
				
				
				
				
			}
			
			
		});
		editStudent.setBounds(582, 76, 116, 23);
		add(editStudent);
		
		JButton showParents = new JButton("Show Parents");
		showParents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*
				EventsTeachersFirstFrame showTeachers = new EventsTeachersFirstFrame();
				showTeachers.setTitle("Add");
				showTeachers.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				if(id==null){
					
				}
				else{
					showTeachers.setId(id);
					showTeachers.updateTheView();
					showTeachers.setVisible(true);
					//showTeachers.setCommends("{call events_package.add_teacher_to_event(?,?)}", "remove", "SELECT * FROM tea_eve order by teachers_id_teacher");
				}
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				*/
				//done
				StudentsParentsFirstFrame showStudents = new StudentsParentsFirstFrame();
				showStudents.setTitle("Add");
			    showStudents.setConnection(connection, statement);
				
				try{
				int selectedRowIndex = table.getSelectedRow();
				int selectedColumnIndex = table.getSelectedColumn();
				BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				
				try{
				id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				if(id==null){
					
				}
				else{
					
					showStudents.setId(id);
					showStudents.updateTheView();
					showStudents.setVisible(true);
					
				}
				}catch(ArrayIndexOutOfBoundsException e){
					JOptionPane.showMessageDialog(null, "First select an event!");
				}
				
				
			 	
			 
			}
		});
		showParents.setBounds(211, 294, 128, 23);
		add(showParents);
		
		JButton deleteStudent = new JButton("Delete Student");
		deleteStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall( "{call events_package.delete_event(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  JOptionPane.showMessageDialog(null, "You have deleted the Event");
					  updateTheView();
					  View.updateTheEventsView(statement,table, tableModel);
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select a event, please");
						}
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		
				}*/
				//done
				int choice =JOptionPane.showConfirmDialog(null, "Do you really want to delete?");
				if(choice==0){
					CallableStatement cs = null;		
					  try { 
					  cs = connection.prepareCall( "{call students_package.delete_student(?)}");
					  int selectedRowIndex = table.getSelectedRow();
					  int selectedColumnIndex = table.getSelectedColumn();
					  BigDecimal id = (BigDecimal) table.getModel().getValueAt(selectedRowIndex, 0);
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  
					  updateTheView();
					  View.updateTheStudentsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call stu_par_package.delete_student(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  
					  updateTheView();
					  View.updateTheStudentsView(statement,table, tableModel);
					  
					  cs = connection.prepareCall( "{call eve_stu_package.delete_student(?)}");
					  cs.setBigDecimal(1, id);
					  cs.execute();
					  
					  updateTheView();
					  View.updateTheStudentsView(statement,table, tableModel);
					  
					   
					   cs = connection.prepareCall( "{call les_stu_package.delete_student(?)}");
						  cs.setBigDecimal(1, id);
						  cs.execute();
						  
						  updateTheView();
						  View.updateTheStudentsView(statement,table, tableModel);
						   JOptionPane.showMessageDialog(null, "You have deleted the Student");
					  } catch (SQLException ee) 
					  { // TODO Auto-generated catch block
					  JOptionPane.showMessageDialog(null,ee.getMessage()); 
					  }
					  catch (ArrayIndexOutOfBoundsException e1){
						  JOptionPane.showConfirmDialog(null, "First select an student, please");
						}
					  try {
						cs.close();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		
				}
			}
		});
		deleteStudent.setBounds(582, 130, 116, 23);
		add(deleteStudent);
	}
	
	public void setConnection(Connection connection, Statement statement){
		this.connection=connection;
		this.statement=statement;
	
	}
	
	public void updateTheView(){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM students order by student_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "StudentsFirstPanel problem");
			
		}
	}
	
}
