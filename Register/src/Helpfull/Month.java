package Helpfull;

public class Month {
	
	public static String getMonth(int i){
		if(i==1){
			return "JAN";
		}
		else if(i==2){
			return "FEB";
		}
		else if(i==3){
			return "MAR";
		}
		else if(i==4){
			return "APR";
		}
		else if(i==5){
			return "MAY";
		}
		else if(i==6){
			return "JUN";
		}
		else if(i==7){
			return "JUL";
		}
		else if(i==8){
			return "AUG";
		}
		else if(i==9){
			return "SEP";
		}
		else if(i==10){
			return "OCT";
		}
		else if(i==11){
			return "NOV";
		}
		else if(i==12){
			return "DEC";
		}
		else{
			return "JAN";
		}
	}
}
