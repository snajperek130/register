package Helpfull;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableModel;

import net.proteanit.sql.DbUtils;

public class View {

	static public void doClick(JFrame frame,JButton buttonToClick){
		frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put
		(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),"clickButton");
		frame.getRootPane().getActionMap().put("clickButton", new AbstractAction(){
	        public void actionPerformed(ActionEvent ae)
	        {
	        	buttonToClick.doClick();
	   
	        }
	    });
	}
	
	static public void doClick(JPanel panel,JButton buttonToClick){
		panel.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put
		(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),"clickButton");
		panel.getRootPane().getActionMap().put("clickButton", new AbstractAction(){
	        public void actionPerformed(ActionEvent ae)
	        {
	        	buttonToClick.doClick();
	   
	        }
	    });
	}
	
	static public void updateTheTachersView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM teachers order by teacher_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "TeacherFirstPanel problem");
			
		}
	}
	
	static public void updateTheEventsView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM events order by event_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "EventsFirstPanel problem");
			
		}
	}
	static public void updateTheLessonsView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT lesson_id, lesson_name,lesson_date from lessons");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			//JOptionPane.showMessageDialog(null, "EventsFirstPanel problem");
			
		}
	}
	
	static public void updateTheParentsView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM parents order by parent_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "ParentsFirstPanel problem");
			
		}
	}
	
	static public void updateTheStudentsView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM students order by student_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "StudentsFirstPanel problem");
			
		}
	}
	
	static public void updateTheTeacherEventFrameView(Statement statement,JTable table, TableModel tableModel){
		try
		{
			ResultSet rs = statement.executeQuery("SELECT * FROM students order by student_id");
			tableModel = DbUtils.resultSetToTableModel(rs);
			table.setModel(tableModel);
			rs.close();
	
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(null, "StudentsFirstPanel problem");
			
		}
	}
		static public void updateEventsTeachersFirstFrameView(Statement statement,JTable table, TableModel tableModel){
			try
			{
				ResultSet rs = statement.executeQuery("SELECT *FROM tea_eve");
				tableModel = DbUtils.resultSetToTableModel(rs);
				table.setModel(tableModel);
			 	rs.close();
		
			}
			catch(Exception ex)
			{
				JOptionPane.showMessageDialog(null, "EventsTeachersFirstFrame problem");
				
			}
	}
	
	  public static Connection getConnection() throws Exception {
		    String driver = "oracle.jdbc.driver.OracleDriver";
		    String url = "jdbc:oracle:thin:@localhost:1521:xe";
		    Class.forName(driver);
		    return DriverManager.getConnection(url, "KONRAD", "olejek1");
		  }

	
}
